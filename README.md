# Empezar con

# Clonar

### `git clone https://gitlab.com/carlosavendano43/test-gisma-app.git`
 
# Instalar

 Instalar el proyecto con el comando

### `npm i`

## Iniciar proyecto

En el directorio del proyecto, se puede ejecutar con el siguiente comando:

### `npm run start`

Ejecuta la aplicación en el modo de desarrollo.
Abre el navegador de su preferencia la siguiente URL [http://localhost:3000](http://localhost:3000)

