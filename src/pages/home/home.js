import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '@mui/material/TextField';
import React, { useState } from 'react';
import PostAddOutlinedIcon from '@mui/icons-material/PostAddOutlined';
import './home.css';
const Home = () => {
    const [list,setList] = useState([]);
    const [newList,setNewList] = useState('');
    const [status,setStatus] = useState(true);
    const onHandleList = (event) => {
        const s = event !== '' ? false:true;
        setStatus(s);
        setNewList(event);     
    }
    const onDelete = (index) => {
        const data = list;
        data.splice(index,1);
        setList([...data]);
    }
    const onAddList = (data) =>{
        setList([...list,data]);
    }
    return(
        <div className="container py-5">
                <div className="row">
                    <div className="col-lg-4">
                            <Card className="mb-3">
                                <CardContent>
                                    <form >
                                        <h2>Formulario</h2>
                                        <TextField className="w-100 mb-3" id="filled-basic" onChange={({target}) => onHandleList(target.value)} label="Ingrese un valor" variant="filled" />
                                        <div className="d-flex justify-content-end">
                                            <Button disabled={status} onClick = {() => onAddList(newList)} size="small">Guardar</Button>
                                        </div>
                                    </form>
                                </CardContent>
                            </Card>
                    </div>
                    <div className="col-lg-8">
                        <Card>
                            <CardContent>
                                <h2>Lista</h2>
                                <ul className="list-group list-group-flush">
                                    {
                                        list.length > 0 ? 
                                        list.map((x,i)=>
                                            <li className="list-group-item d-flex justify-content-between align-items-center" key={i}>
                                                {x}
                                                <div>
                                                    <Tooltip title="Eliminar">
                                                        <IconButton onClick={ () =>onDelete(i)}>
                                                            <DeleteIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                </div>
                                            </li>
                                        ) :
                                        <div>
                                            <div className="d-flex justify-content-center">
                                                <PostAddOutlinedIcon className="font-s-5"/>
                                            </div>
                                            <h5 className="text-center">Agregar un nuevo elemento</h5>
                                        </div>
                                        
                                    }
                                </ul>
                                
                            </CardContent>
                        </Card>
                    </div>
                </div>
            </div>
    )
}
export default Home;