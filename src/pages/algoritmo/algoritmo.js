import React, { useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
const Algoritmo = () => {
    const [res,setRes] = useState(false);
    const [value,setValue] = useState(0);
    const [state,setState] = useState("Falso");
    const onHandleValue = (num) => {
        setRes(false);
        setValue(num);
    }
    const onSetState = () => {
        const s = value && (value & (value - 1)) === 0;
        const response = s ? "Verdadero" : "Falso";
        setState(response);
        if(value > 0 ){
            setRes(true);
        }
    }   
    return(
        <div className="container py-5">
            <div className="row">
                <div className="col-lg-6 mx-auto">
                    <Card>
                        <CardContent>
                            <h2>Algoritmo</h2>
                            <form>
                                <TextField className="w-100 mb-3" type="number" onChange={({target})=>onHandleValue(target.value)} id="filled-basic" label="Ingrese un valor" variant="filled" />
                                <div className="d-flex justify-content-end">
                                    <Button variant="contained" onClick={()=> onSetState()}>Ver</Button>
                                </div>
                            </form>
                            {   res ?
                                <div>
                                    <hr className="my-4"/>
                                    <p>Resultado del valor {value} es potencia de 2: {state}</p>
                                </div>
                                :
                                ""
                            }
                            
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default Algoritmo;