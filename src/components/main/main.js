import { Routes, Route } from "react-router-dom";
/////// pages ////////
import Home from '../../pages/home/home';
import Algoritmo from "../../pages/algoritmo/algoritmo";

const Main = () => {

    return (
        <main>
            <Routes>
                <Route path="/" element={<Home/>}></Route>
                <Route exact path="/algoritmo" element={<Algoritmo/>}></Route>
            </Routes>
        </main>
    )
}

export default Main;