import logo from '../../assets/img/logo/logo.png';
import { Link } from "react-router-dom"
const Header  = () => {
    return(
        <header>
            <nav className="navbar navbar-light bg-light">
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        <img className="img-fluid" src={logo} alt=""/>
                    </Link>
                    <ul className="nav justify-content-end">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/algoritmo">Algoritmo</Link>
                        </li>
                 
                    </ul>
                </div>
            </nav>
        </header>
    )
}
export default Header;