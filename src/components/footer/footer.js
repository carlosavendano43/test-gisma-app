import logo from '../../assets/img/logo/logo.png';

import CallIcon from '@mui/icons-material/Call';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import EmailIcon from '@mui/icons-material/Email';
const Footer = () => {
    return(
        <footer className="bg-light">
            <div className="container py-4">
                <div className="row">
                    <div className="col-lg-4">
                        <img className="img-fluid" src={logo} alt="test"/>
                    </div>
                    <div className="col-lg-4">
                        <h4 className="mb-4">Contacto</h4>
                        <p className="text-muted">Santiago Concha 1148, Santiago Centro, Santiago de Chile</p>
                        <ul className="nav nav-light flex-column">
                            
                            <li className="nav-item">
                                <a className="ps-0 nav-link text-muted" href="mailto:carlosavendano43@gmail.com"><EmailIcon className="pl-2"/>carlosavendano43@gmail.com</a>
                            </li>
                            <li className="nav-item">
                                <a className="ps-0 nav-link text-muted" href="tel:+56936488729"><CallIcon className="pl-2"/>+56936488729</a>
                            </li>
                            <li className="nav-item">
                                <a className="ps-0 nav-link text-muted" rel="noopener noreferrer" href="https://www.linkedin.com/in/carlos-jose-avenda%C3%B1o-mora-32096216a/" target="_blank">
                                    <LinkedInIcon className="pl-2"/>Linkedin
                                </a>
                            </li>
                        </ul>
                        
                    </div>

                    <div className="col-lg-4">
                    <h4 className="mb-4">GISMA</h4>
                    <p className="text-muted">Somos una consultora ambiental chilena líder en innovación para abordar los desafíos de la gestión ambiental y de la sustentabilidad corporativa.</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;