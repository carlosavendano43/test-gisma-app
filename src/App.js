import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router } from "react-router-dom";
import Header from './components/header/header';
import Main from './components/main/main';
import Footer from './components/footer/footer';

const App = () =>{
  return (
    <>
     <Router>
        <Header/>
        <Main/>
        <Footer/>
     </Router>
    </>
  );
}

export default App;
